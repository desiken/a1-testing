## Setup
You open a web browser on your computer. Then, you search for Microsoft Visual Studio. A variety of versions would be available to you. I would suggest 
to download the community version since it is free. You will have to locate the download button and download the software. After installing the software, 
download and open the A1-testing folder from your Bitbucket repository. Once there, you click on the BitbucketTesting.sln file.The project should open 
and you should be able to make changes on the file.

## License
Please, find attached licensing file. 
I selected the MIT licensing for a test purpose for the assignment 1.
